#include <iostream>

#include "Tablica.h"

using namespace std;

void show(const Tablica* s);

int main() {
	Tablica a(12,0);
	show(&a);
	Tablica b=a;
	show(&b);
	a.setElement(10,100-10);
	show(&a);
	b=a;
	show(&b);
   return 0;
   
}
void show(const Tablica* s){
int size=s->getLength();
for (int l=0;l<size;l++){
cout<<s->getElement(l)<<", ";
}
cout<<endl;
}
