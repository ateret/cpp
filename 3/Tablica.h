#ifndef TABLICA_H
#define TABLICA_H

class Tablica{
public:
	Tablica(int length2, long);
	Tablica(const Tablica&);
	~Tablica();
	
	
	Tablica& operator=(Tablica const&);

	
	int getLength() const;
	void setElement(int, long);
	int getElement(int ind) const;

	
private:
	int length;
	long* tab;
	long pocz=0;
	
};


#endif

