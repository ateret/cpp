#include <iostream>
#include <string>
#include "Osoba.h"

using namespace std;

int main() {
    cout << "Ilość osób = " << Osoba::getIle() << endl;

    cout << "Tworzymy mężczyznę" << endl;
    Osoba* pierwszyMezczyzna = new Osoba("Adam","Sandler", 1973 );
    cout << "Ilość osób = " << Osoba::getIle() << endl;
    pierwszyMezczyzna->przedstawSie();

    cout << "Tworzymy kobietę" << endl;
    Osoba* pierwszaKobieta = new Osoba("Ewa","Kopacz", 1978);
    cout << "Ilość osób = " << Osoba::getIle() << endl;
    pierwszaKobieta->przedstawSie();

    cout << "Tworzymy dziecko" << endl;
    Osoba* dziecko = new Osoba("Piotr", "Fronczewski", 1993);
    cout << "Ilość osób = " << Osoba::getIle() << endl;
    dziecko->przedstawSie();
   
Osoba* rodzina[3]={pierwszyMezczyzna, pierwszaKobieta, dziecko};

for (int x=1;x<3;x++){
	rodzina[x]->przedstawSie();

}


 
    cout << "Usuwamy mężczyznę" << endl;
    delete pierwszyMezczyzna;
    cout << "Ilość osób = " << Osoba::getIle() << endl;
}

