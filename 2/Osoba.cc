#include <iostream>
#include "Osoba.h"

using namespace std;

int Osoba::ile = 0;

Osoba::Osoba(const string& imie,const string& nazwisko, int rokUr)
	:imie(imie), nazwisko(nazwisko),rokUr(rokUr)

{
    ++ile;
}


Osoba::~Osoba()
{
    --ile;
}

void Osoba::przedstawSie() const {
    cout << "Mam na imię " + imie<<" " + nazwisko << " i urodzilem sie w ";
    cout << rokUr << " roku." << endl;
}

int Osoba::getIle() {
    return ile;
}
