#ifndef OSOBA_H
#define OSOBA_H

#include <string>

using namespace std;

class Osoba {
    public:
        Osoba(const string& imie,const string& nazwisko, int rokUr);
        ~Osoba(); //destruktor
        void przedstawSie() const;
        static int getIle();
    private:
        static int ile;
//    private:
        string imie;
	string nazwisko;
        int rokUr;
};

#endif
