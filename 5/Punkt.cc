#include <iostream>
#include "Punkt.h"

using namespace std;

int Punkt::ile = 0;

Punkt::Punkt(int x, int y) : x(x), y(y)    
{
++ile;
}


Punkt::~Punkt()
{
--ile;
}


void Punkt::move(int deltaX, int deltaY)
{
x += deltaX;
 y += deltaY;
}


void Punkt::moveTo(int x, int y)
{
this->x = x;
this->y = y;
}


int Punkt::getX() const
{
return x;
}


int Punkt::getY() const
{
return y;
}


void Punkt::show() const
{
cout << "<" << x << "," << y << ">" << endl;
}


int Punkt::getIle()
{
return ile;
}
