#include <iostream>
#include <cstring>
#include "Punkt.h"
#include "NazwanyPunkt.h"

using namespace std;

int main()
    
{    Punkt a(1,2);
    Punkt b(3,4);
    char naz[20]="nazwa";
    NazwanyPunkt p(10,12,naz);
    p.show();
 
    cout<<Punkt::getIle()<<endl;
    cout<<NazwanyPunkt::getIle()<<endl;
    return 0;
}
