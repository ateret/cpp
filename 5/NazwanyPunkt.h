#ifndef NAZWANYPUNKT_H
#define NAZWANYPUNKT_H
#include "Punkt.h"
#include <string>

class NazwanyPunkt: public Punkt
    
{
    
public:
    
	NazwanyPunkt(int,int,char nazwa[16]);
	static int ile;
	void show();
	static int getIle();	
    
private:
    
	char nazwa[16];
	
};




#endif
