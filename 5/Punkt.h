#ifndef PUNKT_H
#define PUNKT_H

class Punkt
{
    
public:
    
    Punkt(int x, int y);
    ~Punkt();
    void move(int deltaX, int deltaY);
    void moveTo(int newX, int newY);
    int getX() const;
    int getY() const;
    void show() const;
    static int getIle();
    
protected:
    
    static int ile;
    int x;
    int y;
};

#endif

