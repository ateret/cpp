// Definicja klasy rozdzielona na deklarację klasy
// oraz definicje metod składowych

#include <iostream>
#include <iomanip>
#include <string> 


using namespace std;

class NazwanyPunkt {
public:
  NazwanyPunkt(int x, int y,string nazwa);
  void move(int deltaX, int deltaY);
  void moveTo(int newX, int newY);
  void show();
private:
  string nazwa;
  int x;
  int y;
};

int main()
{
    NazwanyPunkt a(3, 5,"nazwa1");
    a.show();
    a.move(2, -2);
    a.show();
    // Punkt b; // błąd kompilacji
    NazwanyPunkt b(0, 0,"nazwa2");
    b.show();
    // b.x = 8; // naruszenie hermetyzacji
    // b.show();
}

NazwanyPunkt::NazwanyPunkt(int initX, int initY,string initnazwa)
{
    nazwa = initnazwa;   
    moveTo(initX, initY);
}

void NazwanyPunkt::move(int deltaX, int deltaY)
{
    x += deltaX;
    y += deltaY;
}

void NazwanyPunkt::moveTo(int newX, int newY)
{
    x = newX;
    y = newY;
}

void NazwanyPunkt::show()
{
    cout << nazwa << ": <" << x << "," << y << ">" << endl;
}

