#ifndef WYMIERNA_H
#define WYMIERNA_H

class Wymierna
{
	friend Wymierna operator -(Wymierna const &);
	friend bool operator <(Wymierna const&, Wymierna const &);
	friend bool operator ==(Wymierna const&, Wymierna const &);
	friend double operator +(Wymierna const&, Wymierna const &);
	friend double operator *(Wymierna const&, Wymierna const &);
	
public:
		Wymierna(int, int);
		Wymierna(int);
		Wymierna();
		int getLicznik() const {return p;};
		int getMianownik() const {return q;};	
		double to_double()const;
	
private:
	int p;
	int q;
};
#endif
