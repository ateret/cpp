c#include <iostream>
#include <string>
#include "Wymierna.h"

using namespace std;

Wymierna operator -(Wymierna const & i) {
    Wymierna k(i.p,i.q);
    if(k.p<0 and k.q<0) {
        k.p=-k.p;
        k.q=-k.q;
    }
    else if(k.p>0 and k.q>0) {
        k.p=-k.p;
        k.q=-k.q;
    }

    if (k.p<0 ) {
        k.p=-k.p;
    }
    return k;
}



bool operator <(Wymierna const& r, Wymierna const & t) 
{
    if (r.to_double() <t.to_double())
        return true;
    else
        return false;
}

bool operator ==(Wymierna const& r, Wymierna const & t) 
{
    if (r.to_double() ==t.to_double())
        return true;
    else
        return false;
}

double operator +(Wymierna const& r, Wymierna const &t)
 {
    return (r.to_double() +t.to_double());
}

double operator *(Wymierna const& r, Wymierna const & t) {
    return (r.to_double() *t.to_double());
}



Wymierna::Wymierna(int licznik, int mianownik)
{
p=licznik;
q=mianownik;

   if(p!=0 and (q!=1 or q!=0) ) {

      if (p>0 and q<0) {
           p=-p;
           q=-q;
       }
       else if (p<0 and q<0) {
           p=-p;
           q=-q;
       }
       int a=p;
       int b=q;
       if(a<0)
           a=-a;
       else if(b<0)
            b=-b;
        while(a!=b)
           if(a>b)
               a-=b;
            else
               b-=a;
       int nwd=a;
       p=p/nwd;
       q=q/nwd;
    }

}

Wymierna::Wymierna(int l) {
    Wymierna(l,1);
}

Wymierna::Wymierna() {
    Wymierna(0,1);
}


double Wymierna::to_double()const {
    double wyn;
    wyn=double(p)/double(q);
    return wyn;
}






