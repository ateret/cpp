#ifndef TROJKAT_H
#define TROJKAT_H
#include "Figura.h"

class Trojkat: public Figura 

{
public:
    
    Trojkat(std::string const& ,float,float,float);
    float getPole() const;

    
private:
    
	float a;
	float b;
	float c;
    
};

#endif

