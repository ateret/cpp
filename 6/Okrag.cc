#include <iostream>
#include "Okrag.h"
#include <string>


using namespace std;


Okrag::Okrag(string const& kolor, float r):
Figura(kolor), r(r) {}

float Okrag::getPole() const 
    
{
return 3.14*r*r;
}
