#ifndef PROSTOKAT_H
#define PROSTOKAT_H
#include "Figura.h"

class Prostokat: public Figura 

{
    
public:
    
    Prostokat(std::string const& ,float,float);
    float getPole() const;
    
private:
    
	float x;
	float y;
    
};

#endif

