#ifndef FIGURA_H
#define FIGURA_H

class Figura 

{
    
public:
    
    Figura(std::string const& kolor): kolor(kolor){};
    virtual ~Figura()= default;
    std::string getKolor() const{return kolor;}; 
    virtual float getPole() const=0;
    
    
protected:
    
	std::string kolor;
    
};

#endif
