#include <iostream>
#include "Figura.h"
#include "Okrag.h"
#include "Trojkat.h"
#include "Prostokat.h"

using namespace std;

int main ()
    
{
	Okrag kolo("r",1);
	Prostokat pros("g",2,2);
	Trojkat troj("b",3,4,5);
	Figura *pf[3]={&kolo,&pros,&troj};
	
	for(Figura *a : pf)		
    cout<<a->getKolor()<<" "<<a->getPole()<<endl;
	for(int i=0; i<3; i++)
    delete pf[i];
    return 0;	
}
