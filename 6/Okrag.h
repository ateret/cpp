#ifndef OKRAG_H
#define OKRAG_H
#include "Figura.h"
class Okrag: public Figura 

{
    
public:
    
    Okrag(std::string const& ,float);
    float getPole() const;
    
private:
    
	float r;
    

};

#endif

